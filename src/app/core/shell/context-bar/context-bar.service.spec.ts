import { TestBed, inject } from '@angular/core/testing';

import { ContextBarService } from './context-bar.service';

describe('ContextBarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContextBarService]
    });
  });

  it(
    'should be created',
    inject([ContextBarService], (service: ContextBarService) => {
      expect(service).toBeTruthy();
    })
  );
});
