import {
    Component,
    AfterViewInit
} from '@angular/core';
import {
    ContextBarService
} from './context-bar.service';

@Component({
    selector: 'app-context-bar',
    templateUrl: 'context-bar.component.html',
    styleUrls: ['./context-bar.component.scss']
})
export class ContextBarComponent implements AfterViewInit {

    constructor(private _contextbarService: ContextBarService) {}
    ngAfterViewInit() {
    }


}
