import {
    Injectable,
    ApplicationRef,
    ComponentFactoryResolver,
    Injector,
} from '@angular/core';
import {
    DomPortalHost,
    TemplatePortal
} from '@angular/cdk/portal';

@Injectable()
export class ContextBarService {

    private portalActionHost: DomPortalHost;
    private portalMainHost: DomPortalHost;


    constructor(private componentFactoryResolver: ComponentFactoryResolver,
        private injector: Injector,
        private appRef: ApplicationRef,
    ) {}

    setActionPortal(domLocation, vcr, template) {
        this.portalActionHost = new DomPortalHost(
            document.querySelector(domLocation),
            this.componentFactoryResolver,
            this.appRef,
            this.injector
        );

        const contextBarPortal = new TemplatePortal(
            template,
            vcr
        );

        this.portalActionHost.attach(contextBarPortal);
    }

    setMainPortal(domLocation, vcr, template) {
        this.portalMainHost = new DomPortalHost(
            document.querySelector(domLocation),
            this.componentFactoryResolver,
            this.appRef,
            this.injector
        );

        const contextBarPortal = new TemplatePortal(
            template,
            vcr
        );

        this.portalMainHost.attach(contextBarPortal);
    }

    destoryActionPortal() {
        this.portalActionHost.detach();
    }

    destoryMainPortal() {
        this.portalMainHost.detach();

    }

    isAttached() {
        if (this.portalActionHost || this.portalMainHost) {
            return true
        } else {
            return false
        }
    }

}
