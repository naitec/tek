import {
    ApplicationRef,
    Injector,
    OnInit,
    ViewContainerRef,
    Directive,
    OnDestroy,
    TemplateRef
} from '@angular/core';
import {
    ContextBarService
} from './context-bar.service';

@Directive({
    selector: '[appContextBarMain]'
})

export class ContextBarMainDirective implements OnInit, OnDestroy {
    constructor(private _contextBarService: ContextBarService,
        private viewContainerRef: ViewContainerRef,
        public tpl: TemplateRef < any > ) {}

    ngOnInit() {
        if (this.tpl) {
            this._contextBarService.setMainPortal('#contextbarmain', this.viewContainerRef, this.tpl);
        }
    }

    ngOnDestroy() {
        if (this.tpl) {
            this._contextBarService.destoryMainPortal();
        }
    }

}
